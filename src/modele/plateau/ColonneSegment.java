package modele.plateau;

import modele.deplacements.Direction;

public class ColonneSegment extends EntiteDynamique {
    private Colonne col;
    public ColonneSegment(Jeu _jeu, Colonne _col) {
        super(_jeu);
        col = _col;
    }

    @Override
    public boolean avancerDirectionChoisie(Direction d) {
        return super.avancerDirectionChoisie(d);
    }

    @Override
    public Entite regarderDansLaDirection(Direction d) {
        return super.regarderDansLaDirection(d);
    }

    @Override
    public boolean peutEtreEcrase() {
        return false;
    }

    @Override
    public boolean peutServirDeSupport() {
        return true;
    }

    @Override
    public boolean peutPermettreDeMonterDescendre() {
        return false;
    }
}
